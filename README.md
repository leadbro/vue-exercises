## Задание для Сергея

```
1. Склонировать репозиторий
2. Создать ветку development
3. Доработать компонент HomeTable:
  а) Таблица строится на основе массива products
  б) Заголвок таблицы содержит кнопки, вызывающие метод сортировки массива (реверсию пока не надо)
  в) Симпатично стилизовать
4. Сделать коммит
5. Запушить во внешний репозиторий
```


### Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```
